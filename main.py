# -*- coding:utf-8 -*-

from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
from hashlib import sha1
from requests import post

import random
import time
import hmac
import base64

# pip install requests_toolbelt
from requests_toolbelt.multipart.encoder import MultipartEncoder


def aesEncrypt(data, key):
    # iv = "0000000000000000"
    strBase = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    iv = ''.join(random.sample(strBase, 16))
    ivBytes = iv.encode("utf-8")
    dataBytes = data
    keyBytes = key.encode("utf-8")
    cipher = AES.new(keyBytes, AES.MODE_CBC, ivBytes)
    encryptData = cipher.encrypt(pad(dataBytes, AES.block_size))
    return base64.b64encode(encryptData+ivBytes).decode("utf-8")


def hmacSHA1(data, key):
    dataBytes = data.encode("utf-8")
    keyBytes = key.encode("utf-8")
    encryptData = hmac.new(keyBytes, dataBytes, digestmod=sha1).digest()
    return base64.b64encode(encryptData).decode("utf-8")


def main():
    api = "https://t-cloud-api-alpha.niutr.com/car-version/external/service/api/v1/log"
    accessKey = "4N4zkoJ8HC5sukcC"
    secretKey = "urwzj9HxopnERJFH54ZIMfw0T66NKUYj"

    multipartForm = MultipartEncoder({
        'vin': 'LF0E6PTE9N1000163',
        'log': ('test.log', open('test.log', 'rb'), 'text/plain'),
    })
    sourceBody = multipartForm.to_string()
    timestamp = str(int(time.time() * 1000))
    base64Body = aesEncrypt(sourceBody, secretKey)
    sign = hmacSHA1(base64Body + timestamp, secretKey)

    print("source body =", sourceBody)
    print("base64 body =", base64Body)
    print("timestamp   =", timestamp)
    print("sign        =", sign)

    headers = {
        'Content-Type': multipartForm.content_type,
        'x-niutron-sign': sign,
        'x-niutron-ak': accessKey,
        'timestamp': timestamp
    }
    res = post(url=api, data=base64Body, headers=headers)
    print(res.text)


if __name__ == '__main__':
    main()

# 执行结果:
# heyi@heyideMacBook-Pro open-api$ python3 main.py
# source body = {"test_key":"test_value"}
# base64 body = 0YGHOptOBt4STnjZpobT/RYefp2+HToqrv+YyZ9B2/VmcmJ3ODJ6bWpNNEx4b3Bu
# timestamp   = 1646292794
# sign        = k1Zl4r0F9y8bHDzUMNwKKwS210erSrDNgy24j7QzZXA=
# {"code":0,"msg":"OK","data":{"test_key":"test_value"}}
