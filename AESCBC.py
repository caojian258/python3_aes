import base64
import binascii
import re
from Crypto.Cipher import AES


class AESCBC:
    def __init__(self):
        self.key = b'dsa@tm3.file6868'  # 定义key值
        self.iv = b'8760432787651324'  # 定义iv值
        self.mode = AES.MODE_CBC
        self.bs = 16  # block size
        self.PADDING = lambda s: s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs).encode('utf-8')

    def encrypt(self, text):
        generator = AES.new(self.key, self.mode, self.iv)  # 这里的key 和IV 一样 ，可以按照自己的值定义
        print('原文' + text)
        btext = text.encode()
        print(btext)
        print(self.PADDING(btext))
        crypt = generator.encrypt(self.PADDING(btext))
        # crypted_str = base64.b64encode(crypt)   #输出Base64
        print(crypt)
        crypted_str = binascii.b2a_hex(crypt)  # 输出Hex
        result = crypted_str.decode()
        return result

    def decrypt(self, text):
        generator = AES.new(self.key, self.mode, self.iv)
        #text += (len(text) % 4) * '='
        decrpyt_bytes = base64.b64decode(text)           #输出Base64
        #decrpyt_bytes = binascii.a2b_hex(text)  # 输出Hex
        meg = generator.decrypt(decrpyt_bytes)
        # 去除解码后的非法字符
        try:
            result = re.compile('[\\x00-\\x08\\x0b-\\x0c\\x0e-\\x1f\n\r\t]').sub('', meg.decode())
        except Exception:
            result = '解码失败，请重试!'
        return result


if __name__ == '__main__':
    aes = AESCBC()

    # to_encrypt = '''{"msgType":"text","touser":"liaodongsheng-ghq","msgData":{"content":"安全 from 测试"}}'''
    to_encrypt = '''123456890'''
    to_decrypt = 'VWejEe8Mf2HIj1U1KQqFcg=='
    # to_decrypt = 'fd0405f8efa4495654cf07b8d3a95354'
    # 解密不需要偏移量

    print("\n加密前:{0}\n加密后:{1}\n".format(to_encrypt, aes.encrypt(to_encrypt)))
    print("解密前:{0}\n解密后：{1}".format(to_decrypt, aes.decrypt(to_decrypt)))