
import requests
import time
import os
import sys


class Downloader(object):
   def __init__(self, url, fileName, fileUrl, token, file_path):
      self.url = url
      self.file_path = file_path
      self.fileName = fileName
      self.fileUrl = fileUrl
      self.token = token

   def start(self):

      # JSON格式body数据

      body = {
         "name": 'XP_Skylyze_user_manual.zip',
         "url":  '07/10/XP_Skylyze_user_manual.zip'
      }

      headers = {'Range': 'bytes=1024-',
                 'Content-Type': 'application/json;charset=UTF-8',
                 'Token': self.token,
                 "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0"}

      res_length = requests.post(self.url, stream=True, headers=headers, json=body)
      total_size = int(res_length.headers['Content-Length'])
      print(res_length.headers)
      print(res_length.status_code)

      if os.path.exists(self.file_path):
         temp_size = os.path.getsize(self.file_path)
         print("当前：%d 字节， 总：%d 字节， 已下载：%2.2f%% " % (temp_size, total_size, 100 * temp_size / total_size))
      else:
         temp_size = 0
         print("总：%d 字节，开始下载..." % (total_size,))

      headers = {'Range': 'bytes=%d-' % temp_size,
                 'Content-Type': 'application/json;charset=UTF-8',
                 'Token': self.token,
                 "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0"}

      res_left = requests.post(self.url, stream=True, headers=headers, json=body)

      with open(self.file_path, "ab") as f:
         for chunk in res_left.iter_content(chunk_size=1024):
            temp_size += len(chunk)
            f.write(chunk)
            f.flush()

            done = int(50 * temp_size / total_size)
            sys.stdout.write("\r[%s%s] %d%%" % ('█' * done, ' ' * (50 - done), 100 * temp_size / total_size))
            sys.stdout.flush()


if __name__ == '__main__':
   # header = {
   #    "Content-Type": "application/json;charset=UTF-8",
   #    'Token': '3bf4a544b081fb1ae3f8d0ede2a1ebba'
   # }
   # body = {  # JSON格式body数据
   #    "name": "sqlcipher.txt",
   #    "url": "07/10/sqlcipher.txt"
   # }
   down_url = 'http://localhost:8082/dsaPlatformApi/ecu/flash/downloadFile'
   #
   # res = requests.post(down_url, headers=header, stream=True, json=body).content
   # with open("C:/Users/jian8/Downloads/test/sqlcipher.txt", "wb") as f:
   #    f.write(res)
   path = 'C:/Users/jian8/Downloads/test/XP_Skylyze_user_manual.zip'
   name = 'XP_Skylyze_user_manual.zip',
   url = '07/10/XP_Skylyze_user_manual.zip'
   token = '3bf4a544b081fb1ae3f8d0ede2a1ebba'
   downloader = Downloader(down_url, name, url,  token, path)
   downloader.start()