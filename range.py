import base64
import os
import sys
import random

from crypto.Cipher import AES
from crypto.Util.Padding import pad
import requests

def aesEncrypt(data, key):
    # iv = "0000000000000000"
    strBase = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    iv = ''.join(random.sample(strBase, 16))

    ivBytes = iv.encode("utf-8")
    dataBytes = bytes(data, encoding="utf-8")
    keyBytes = key.encode("utf-8")
    cipher = AES.new(keyBytes, AES.MODE_CBC, ivBytes)
    encryptData = cipher.encrypt(pad(dataBytes, AES.block_size))
    return base64.b64encode(encryptData+ivBytes).decode("utf-8")

def aesDecrypt(data, key):
    strBase = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    iv = ''.join(random.sample(strBase, 16))
    ivBytes = iv.encode("utf-8")
    dataBytes = data
    keyBytes = key.encode("utf-8")
    cipher = AES.new(keyBytes, AES.MODE_CBC, ivBytes)


def file_size(filename):
    return os.stat(filename).st_size


def download(url, chunk_size=65535):
    downloaded = 0  # How many data already downloaded.
    filename = url.split('/')[-1]  # Use the last part of url as filename
    print(filename)
    if os.path.isfile(filename):
        downloaded = file_size(filename)
        print("File already exists. Send resume request after {} bytes".format(
            downloaded))

    # Update request header to add `Range`
    headers = {}
    if downloaded:
        headers['Range'] = 'bytes={}-'.format(downloaded)
        headers['token'] = '466a2ee151830b40c9bb3781966522c9'
    res = requests.get(url, headers=headers, stream=True, timeout=15)

    mode = 'w+'
    content_len = int(res.headers.get('content-length'))
    print("{} bytes to download.".format(content_len))
    # Check if server supports range feature, and works as expected.
    if res.status_code == 206:
        # Content range is in format `bytes 327675-43968289/43968290`, check
        # if it starts from where we requested.
        content_range = res.headers.get('content-range')
        # If file is already downloaded, it will reutrn `bytes */43968290`.

        if content_range and \
                int(content_range.split(' ')[-1].split('-')[0]) == downloaded:
            mode = 'a+'
    if res.status_code == 416:
            print("File download already complete.")
            return

    # with open(filename, mode) as fd:
    #     for chunk in res.iter_content(chunk_size):
    #         if len(chunk)>0:
    #             fd.write(chunk))
    #             downloaded += len(chunk)
    #             print("{} bytes downloaded.".format(downloaded))

    print("Download complete.")


if __name__ == '__main__':
    #url = 'http://101.132.27.25:8001/dsaPlatformApi/ota/flash/downloadFile?params=5ZD2fwqGoPcNeN1S9KXdLyQIDBAGElCX62wg1HdWxRx4r10%2F87fyESc9kcnUPwBGdzsBMlz%2FlXo87bwey6ggcBiEPdjX%2BBh6IfQO%2BnG%2FpOo%3D'
    #download(url)
    data = 'okokssss'
    key = "dsa@tm3.file6868"
    result = aesEncrypt(data, key)
    print(result)